package stack;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {

	StackItem<T> top;
	
	@Override
	public void push(T item) {
		StackItem<T> newTop = new StackItem<>(item, top);
		top = newTop;
	}

	@Override
	public T pop() {
		T oldTop = top.getItem();
		top = top.getNext();

		return oldTop;
	}

	@Override
	public boolean empty() {
		return top == null;
	}

	@Override
	public List<T> toList() {
		List<T> content = new ArrayList<>();
		StackItem<T> current = top;
		while (current != null) {
			content.add(0, current.getItem());
			current = current.getNext();
		}
		return content;
	}

	@Override
	public void addAll(Stack<T> aStack) {
		
	}
	
}
