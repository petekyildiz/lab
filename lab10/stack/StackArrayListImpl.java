package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	ArrayList<T> content = new ArrayList<>();
	
	@Override
	public void push(T item) {
		content.add(item);
	}

	@Override
	public T pop() {
		return content.remove(content.size() - 1);
	}

	@Override
	public boolean empty() {
		return content.size() == 0;
	}

	@Override
	public List<T> toList() {
		return content;
	}

	@Override
	public void addAll(Stack<T> aStack) {
		
	}
	
}
