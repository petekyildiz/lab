package stack;

public class StackItem<T> {

	private T item;
	private StackItem<T> next;
	
	public StackItem (T item, StackItem<T> next) {
		this.item = item;
		this.next = next;
	}
	
	public T getItem () {
		return item;
	}
	
	public StackItem<T> getNext () {
		return next;
	}
}
