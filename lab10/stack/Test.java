package stack;

public class Test {

	public static void main(String[] args) {
		
		Stack<String> stack1 = new StackArrayListImpl<>();
		stack1.push("A");
		stack1.push("B");
		stack1.push("C");
		if (!stack1.empty())
			System.out.println(stack1.pop());
		if (!stack1.empty())
			System.out.println(stack1.pop());
		stack1.push("D");
		stack1.push("E");
		System.out.println(stack1.toList());
		
		Stack<String> stack2 = new StackImpl<>();
		stack2.push("F");
		stack2.push("G");
		stack2.push("H");
		if (!stack2.empty())
			System.out.println(stack2.pop());
		System.out.println(stack2.toList());
		
	}

}
