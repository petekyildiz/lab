package drawingV2;

public class Square {

	private double side;
	
	public Square (double side) {
		this.side = side;
	}
	
	public double area () {
		return Math.pow(side, 2);
	}
}
