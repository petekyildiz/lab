package drawingV2;

import java.util.ArrayList;

public class Drawing {

	private ArrayList<Object> shapes = new ArrayList<>();

	public double calculateTotalArea () {
		double totalArea = 0;
		
		for (Object object : shapes) {
			if (object instanceof Circle) {
				Circle circle = (Circle) object;
				totalArea += circle.area();
			} else if (object instanceof Rectangle) {
				Rectangle rect = (Rectangle) object;
				totalArea += rect.area();
			} else if (object instanceof Square) {
				Square square = (Square) object;
				totalArea += square.area();
			}
		}
		return totalArea;
	}
	
	public void addCircle (Circle circle) {
		shapes.add(circle);
	}
	
	public void addRectangle (Rectangle rect) {
		shapes.add(rect);
	}
	
	public void addSquare (Square square) {
		shapes.add(square);
	}
}
