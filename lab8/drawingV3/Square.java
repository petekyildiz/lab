package drawingV3;

public class Square extends Shape {

	private double side;
	
	public Square (double side) {
		this.side = side;
	}
	
	public double area () {
		return Math.pow(side, 2);
	}
	
	public double perimeter () {
		return 4 * side;
	}
}
