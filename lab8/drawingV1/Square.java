package drawingV1;

public class Square {

	private double side;
	
	public Square (int side) {
		this.side = side;
	}
	
	public double area () {
		return Math.pow(side, 2);
	}
}
