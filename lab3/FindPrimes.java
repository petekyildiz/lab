
public class FindPrimes {

  public static void main(String[] args) {

    int number = Integer.parseInt(args[0]);
    int counter = 0;
    boolean firstNumber = true;

    for (int i = 2; i < number; i++) {
      for (int j = 2; j < i; j++) {
        //System.out.println("i: "+ i + " j: " + j);
        if (i % j == 0) {
            counter++;
        }
      }
      // no divisor, number is prime
      if (counter == 0) {
        if (firstNumber) {
          System.out.print(i);
          firstNumber = false;
        } else {
          System.out.print("," + i);
        }
      }
      counter = 0;
    }
    System.out.println();
  }
}
