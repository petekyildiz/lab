
public class MatrixCalculator {

  public static void main(String[] args) {

    int[][] matrixA = {{6, 8, 2}, {9, 5, 11}, {7, 2, 5}};
    int[][] matrixB = {{4, 6, 3}, {5, 8, 1}, {6, 6, 7}};

    int[][] result = matrixAddition(matrixA, matrixB);

    System.out.print("result = {");
    for (int i = 0; i < result.length; i++) {
      System.out.print("{");
      for (int j = 0; j < result[0].length; j++) {
        System.out.print(" " + result[i][j] + " ");
      }
      System.out.print("}");
    }
    System.out.println("}");
  }

  public static int[][] matrixAddition(int[][] matrixA, int[][] matrixB) {

    int m = matrixA.length;
    int n = matrixA[0].length;
    int[][] result = new int[m][n];

    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        result[i][j] = matrixA[i][j] + matrixB[i][j];
      }
    }

    return result;
  }
}
