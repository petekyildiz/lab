
public class GreatestDemo {

    public static void main(String[] args) {

      int value1 = Integer.parseInt(args[0]);
      int value2 = Integer.parseInt(args[1]);
      int value3 = Integer.parseInt(args[2]);
      int result;

      boolean condition1 = value1 > value2;
      boolean condition2 = value1 > value3;
      boolean condition3 = value2 > value3;

      result = condition1 ? (condition2 ? value1 : value3) : (condition3 ? value2 : value3);

      System.out.println(result);
    }
}
