package petek.main;

import petek.shapes.Circle;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Circle circ1 = new Circle(3);
		Circle circ2 = new Circle(4);
		Circle circ3 = new Circle(5);
		
		ArrayList<Circle> circArray = new ArrayList<>();
		circArray.add(circ1);
		circArray.add(circ2);
		circArray.add(circ3);
		
		for (int i = 0; i < circArray.size(); i++) {
			System.out.println("area of " + (i + 1) + ". circle: " + circArray.get(i).area());
		}
	}
}
