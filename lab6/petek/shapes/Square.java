package petek.shapes;

public class Square {
	
	protected int width;
	
	public Square (int widht) {
		this.width = widht;
	}
	
	public int area () {
		return width * width;
	}
	
	public int perimeter () {
		return 4 * width;
	}
}
