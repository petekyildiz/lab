package petek.shapes3d;

import petek.shapes.Square;

public class Cube extends Square {

	private int height;
	
	public Cube (int width) {
		super(width);
		this.height = width;
	}
	
	@Override
	public int area () {
		return 6 * super.area();
	}
	
	public int volume () {
		return super.area() * height;
	}
	
	@Override
	public String toString () {
		return "width: " + super.width;
	}
}
