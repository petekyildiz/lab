package petek.shapes3d;

import petek.shapes.Circle;

public class Cylinder extends Circle {

	private int height;
	
	public Cylinder (int radius, int height) {
		super(radius);
		this.height = height;
	}
	
	@Override
	public double area () {
		return 2 * super.area() + (super.perimeter() * height);
	}
	
	public double volume () {
		return super.area() * height;
	}
	
	@Override
	public String toString () {
		return "radius: " + radius + ", height: " + height;
	}
}
