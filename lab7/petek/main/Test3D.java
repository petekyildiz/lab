package petek.main;

import petek.shapes3d.Cube;
import petek.shapes3d.Cylinder;

public class Test3D {

	public static void main (String[] args) {
		
		Cylinder cylinder = new Cylinder (3, 5);
		System.out.println("cylinder object is created.");
		System.out.println(cylinder);
		System.out.println("area: " + cylinder.area());
		System.out.println("volume: " + cylinder.volume());
		
		Cube cube = new Cube(4);
		System.out.println("\n" + "cube object is created.");
		System.out.println(cube);
		System.out.println("area: " + cube.area());
		System.out.println("volume: " + cube.volume());
	}
}
