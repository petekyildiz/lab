public class GCDLoop {

	public static void main (String[] args) {

		if (args.length < 2) {
			System.out.println("Please enter two numbers!");
			
		} else {
			int firstNumber = Integer.parseInt(args[0]);
			int secondNumber = Integer.parseInt(args[1]);
			
			System.out.println(GCD(firstNumber, secondNumber));
		}
	}

	public static int GCD (int a, int b) {
		
		// keep greater number as first number
		if (a < b) {
			int swap = a;
			a = b;
			b = swap;
		}
				
		while (b != 0) {
			int c = a;
			a = b;
			b = c % b;			
		}
		
		return a;
	}
}
