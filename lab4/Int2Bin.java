public class Int2Bin {

	public static void main (String[] args) {

		if (args.length < 2) {
			System.out.println("Usage: [loop|rec] number");
			
		} else {
			int number = Integer.parseInt(args[1]);
			
			if (args[0].equals("loop")) {
				System.out.println(loop(number));
			} else if (args[0].equals("rec")) {
				System.out.println(rec(number));
			} else {
				System.out.println("Usage: [loop|rec] number");
			}
		}
	}
	
	public static String loop (int number) {
		
		String binary = "";
		
		while (number > 0) {
			binary = ((number % 2) == 0 ? "0" : "1") + binary;
			number = number / 2;
		}
		
		return binary;
	}
	
	public static String rec (int number) {
				
		if (number == 0) {
			return "";
		}
		
		return rec(number / 2) + ((number % 2) == 0 ? "0" : "1");
	}
}
