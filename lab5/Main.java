
public class Main {

	public static void main(String[] args) {

		Rectangle rect = new Rectangle();
		
		rect.sideA = 5;
		rect.sideB = 6;
		
		System.out.println("Area of rectangle: " + rect.area());
		System.out.println("Perimeter of rectangle : " + rect.perimeter());
		
		Circle circ = new Circle(10);

		System.out.println("Area of circle: " + circ.area());
		System.out.println("Perimeter of circle: " + circ.perimeter());
	}

}
